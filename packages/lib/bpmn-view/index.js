import BpmnView from "./src/main.vue";

BpmnView.install = function (app) {
  app.component(BpmnView.name, BpmnView);
};

export default BpmnView;
