import BpmnDesigner from "./src/main.vue";

BpmnDesigner.install = function (app) {
  app.component(BpmnDesigner.name, BpmnDesigner);
};

export default BpmnDesigner;
