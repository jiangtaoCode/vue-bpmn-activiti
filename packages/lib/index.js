import Demo from "./demo";
import BpmnDesigner from "./bpmn-designer";
import BpmnView from "./bpmn-view";

const components = { Demo, BpmnDesigner, BpmnView };

const install = function (app) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach((key) => {
    const component = components[key];
    app.component(component.name, component);
  });
};

export default {
  install,
};
