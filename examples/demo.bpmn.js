export default `<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="diagram_Process_1644737873066" targetNamespace="http://activiti.org/bpmn">
  <bpmn2:process id="Process_1644737873066" name="业务流程_1644737873066" isExecutable="true">
    <bpmn2:startEvent id="Event_09ccqp5">
      <bpmn2:outgoing>Flow_0h0icd5</bpmn2:outgoing>
    </bpmn2:startEvent>
    <bpmn2:userTask id="Activity_07td992" name="组长审批">
      <bpmn2:incoming>Flow_0h0icd5</bpmn2:incoming>
      <bpmn2:outgoing>Flow_1lhz2tc</bpmn2:outgoing>
    </bpmn2:userTask>
    <bpmn2:sequenceFlow id="Flow_0h0icd5" sourceRef="Event_09ccqp5" targetRef="Activity_07td992" />
    <bpmn2:userTask id="Activity_1bj9xl3" name="经理审批">
      <bpmn2:incoming>Flow_1lhz2tc</bpmn2:incoming>
      <bpmn2:outgoing>Flow_1s88jqk</bpmn2:outgoing>
    </bpmn2:userTask>
    <bpmn2:sequenceFlow id="Flow_1lhz2tc" sourceRef="Activity_07td992" targetRef="Activity_1bj9xl3" />
    <bpmn2:endEvent id="Event_1w1dma1">
      <bpmn2:incoming>Flow_1s88jqk</bpmn2:incoming>
    </bpmn2:endEvent>
    <bpmn2:sequenceFlow id="Flow_1s88jqk" sourceRef="Activity_1bj9xl3" targetRef="Event_1w1dma1" />
  </bpmn2:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1644737873066">
      <bpmndi:BPMNEdge id="Flow_0h0icd5_di" bpmnElement="Flow_0h0icd5">
        <di:waypoint x="-12" y="240" />
        <di:waypoint x="40" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1lhz2tc_di" bpmnElement="Flow_1lhz2tc">
        <di:waypoint x="140" y="240" />
        <di:waypoint x="200" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1s88jqk_di" bpmnElement="Flow_1s88jqk">
        <di:waypoint x="300" y="240" />
        <di:waypoint x="362" y="240" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Event_09ccqp5_di" bpmnElement="Event_09ccqp5">
        <dc:Bounds x="-48" y="222" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_07td992_di" bpmnElement="Activity_07td992">
        <dc:Bounds x="40" y="200" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1bj9xl3_di" bpmnElement="Activity_1bj9xl3">
        <dc:Bounds x="200" y="200" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_1w1dma1_di" bpmnElement="Event_1w1dma1">
        <dc:Bounds x="362" y="222" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn2:definitions>
`;
