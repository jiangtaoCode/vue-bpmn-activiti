import { createApp } from "vue";
import App from "./App.vue";

import "@jt_coder/vue-bpmn-activiti/dist/index.css";
import VueBpmn from "@jt_coder/vue-bpmn-activiti";

// import VueBpmn from "../packages/lib";

const app = createApp(App);

app.use(VueBpmn);

app.mount("#app");
